// after seeing many 300 line solutions.
// here's a single line.
const ALPHABET_SOUP=p=>((nxn,...P)=>((_,
m,W,A)=>W.map(w=>((f,v,e)=>console.log(w
,(L=c=>f((Z=(a,b)=>A(a,(_,i)=> Array(b).
fill(i)))(x,y))[e][c]+":"+(f(T(Z(y,x)))[
e][c]))(v),L(v+w.length-1)))(...[[m=>m],
[ R= m=>m                      .map(r=>[
...r]["r"                      +"everse"
]())],[T=m=>m.map((_,i)=>m.map(a=>a[i]))
],[R,T],[D=m=>A(s=y+x-1,(_,i)=>A(x,(_ ,j
)=>(m[s-i              -j-1] ||[] )[j]).
map(x=> x              !=undefined?x:' '
))],[D,R]    ,[R,D],[R,D,R]].reduce((k,F
)=>k?k:(N    =(f=q=>F.reduce((a,b)=>b(a)
,q) )(m).    reduce((c,r,i)=> c?c : (l =
r.join(''                      ).indexOf
(w))>=0?[                      l,i]:0,0)
)?[f, ...N]:0,0))))([x,y]=nxn.split('x')
.map(p=>parseInt(p)) ,P.slice(0,x ).map(
x=>x.split(' ')),P.slice(x),(d,Q)=>Array
(d ).fill().map(Q) ))(...p.split( '\n'))

// tests
ALPHABET_SOUP(`3x3
A B C
D E F
G H I
ABC
AEI`)

ALPHABET_SOUP(`5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE`)

// this might be the only pr to reduce
// the line count.  done as a joke.  i
// don't  actually want this job, just
// sharing.
